"""macshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import include, path
from django.contrib.staticfiles.urls import static, staticfiles_urlpatterns
from django_registration.backends.one_step.views import RegistrationView
from django.contrib.auth.views import LogoutView, LoginView
from django.contrib.auth import views as auth_views

from app.views import Index, ProfileView, ProductsView, ProductDetailView, FilterMacBookView, ReviewsView, HelpView, \
    ContactsView, SimpleBuyView, ConfirmView, AddToCartView, CartItemRemoveView, OrderingView, PurchaseHistoryView
from app.forms import CustomUserForm

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Index.as_view()),
    path('accounts/register/', RegistrationView.as_view(success_url='/', form_class=CustomUserForm),
         name='django_registration_register'),
    path('accounts/', include('django_registration.backends.activation.urls')),
    path('accounts/login/', LoginView.as_view(), name='login'),
    path('accounts/profile/', ProfileView.as_view(), name='profile'),
    path('accounts/logout/', LogoutView.as_view(), name='logout'),
    path('change-password/',
         auth_views.PasswordChangeView.as_view(template_name='registration/password_change_form.html'),
         name='password_change'),
    path('change-password/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('products/<int:pk>', ProductsView.as_view(), name='products'),
    path('product_detail/<int:pk>', ProductDetailView.as_view(), name='product_detail'),
    path('filter/', FilterMacBookView.as_view(), name='filter'),
    path('help/', HelpView.as_view(), name='help'),
    path('contacts/', ContactsView.as_view(), name='contacts'),
    path('reviews/', ReviewsView.as_view(), name='reviews'),
    path('simple_buy/<int:pk>', SimpleBuyView.as_view(), name='simple_buy'),
    path('confirm/', ConfirmView.as_view(), name='confirm'),
    path('add_to_cart/<int:pk>', AddToCartView.as_view(), name='add_to_cart'),
    path('remove_item', CartItemRemoveView.as_view(), name='remove_item'),
    path('ordering', OrderingView.as_view(), name='ordering'),
    path('purchase_history', PurchaseHistoryView.as_view(), name='purchase_history'),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
                      path('__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
