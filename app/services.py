from app.models import Category, MacBook, Tag, Cart, Reviews, Order


def _get_categories():
    """Получение всех имеющихся категорий продукта"""
    return Category.objects.all().order_by('display')


def _get_macbook(pk=None):
    """"Возвращает продукта по заданной модели"""
    if pk == 0:
        return MacBook.objects.filter(availability=True).select_related().prefetch_related('macbookimage_set')
    else:
        return MacBook.objects.filter(name__id=pk, availability=True).select_related().prefetch_related('macbookimage_set')


def _get_years():
    """Получение годов продуктов"""
    return MacBook.objects.filter(availability=True).values_list("year", flat=True).order_by("year").distinct()


def _get_tags():
    """Получение всех тегов"""
    return Tag.objects.all().order_by("tag").distinct()


def _get_filtered_macbooks(cpu, display, ram, hdd, ssd, condition, year, tags, order_by, category_id=0):
    """Получение продуктов по заданным фильтрам"""
    queryset = MacBook.objects.filter(availability=True).distinct().select_related().prefetch_related('macbookimage_set')
    if category_id and category_id != '0':
        queryset = queryset.filter(name__id=category_id)
    if cpu:
        queryset = queryset.filter(cpu__in=cpu)
    if display:
        queryset = queryset.filter(display__in=display)
    if ram:
        queryset = queryset.filter(ram__in=ram)
    if hdd:
        queryset = queryset.filter(hdd__in=hdd)
    if ssd:
        queryset = queryset.filter(ssd__in=ssd)
    if condition:
        queryset = queryset.filter(condition__in=condition)
    if year:
        queryset = queryset.filter(year__in=year)
    if tags:
        queryset = queryset.filter(tags__in=tags)
    if order_by:
        queryset = queryset.order_by(order_by)
    return queryset


def _get_current_cart_products(user_id):
    """Продукты в корзине"""
    return Cart.objects.filter(user_id=user_id, processed=False).select_related().prefetch_related(
        'product__macbookimage_set')


def _get_current_cart_final_price(user_id):
    """Цена продуктов в корзине"""
    return sum(Cart.objects.values_list('product__price', flat=True).filter(user_id=user_id, processed=False))


def _get_reviews():
    """Отзывы"""
    reviews = Reviews.objects.all().order_by('-created_at').select_related()
    if reviews:
        return reviews


def _get_orders_history(user):
    """История заказов пользователя"""
    return Order.objects.filter(user=user).prefetch_related(
        'products__product__macbookimage_set', ).select_related().order_by('-created_at')


def _create_order(request):
    """Создание заказа"""
    order = Order(user=request.user, delivery_method=request.POST['delivery_method'],
                  first_name=request.POST['first_name'],
                  last_name=request.POST['last_name'], region=request.POST['region'],
                  city=request.POST['city'],
                  phone_number=request.POST['phone_number'])
    order.address = request.POST['address']
    if request.POST['post_office_number']:
        order.post_office_number = request.POST['post_office_number']
    order.final_price = _get_current_cart_final_price(request.user.id)
    order.save()
    order.products.add(*Cart.objects.filter(user=request.user,
                                            processed=False))
    Cart.objects.filter(user=request.user, processed=False).update(processed=True)
