from django_registration.forms import RegistrationForm
from django import forms
from django.core.validators import RegexValidator

from .models import CustomUser, OneClickPurchaseRequisitions, Order, Reviews


class CustomUserForm(RegistrationForm):
    class Meta(RegistrationForm.Meta):
        model = CustomUser


class SimpleBuyForm(forms.ModelForm):
    class Meta:
        model = OneClickPurchaseRequisitions
        fields = (
            'phone_number',
        )

    def clean_phone_number(self):
        if not self.cleaned_data['phone_number'].isdigit():
            raise forms.ValidationError("Номер телефона не должен содержать буквы или спец символы")
        elif len(self.cleaned_data['phone_number']) != 10:
            raise forms.ValidationError("Номер телефона введен некоректно")
        return self.cleaned_data['phone_number']


class ProfileForm(SimpleBuyForm):
    phone_number = forms.CharField(label='Номер мобильного телефона')
    email = forms.EmailField(label='Адрес электронной почты')
    address = forms.CharField(label='Адрес доставки', required=False)
    post_office_number = forms.IntegerField(label='Номер почтового отделения', required=False)
    first_name = forms.CharField(label='Имя', validators=[RegexValidator(regex=r'^[a-zA-Zа-яА-Я]+$')])
    last_name = forms.CharField(label='Фамилия', validators=[RegexValidator(regex=r'^[a-zA-Zа-яА-Я]+$')])
    region = forms.CharField(label='Область', validators=[RegexValidator(regex=r'^[a-zA-Zа-яА-Я]+$')])
    city = forms.CharField(label='Город', validators=[RegexValidator(regex=r'^[a-zA-Zа-яА-Я]+$')])

    class Meta:
        model = CustomUser
        fields = (
            'first_name', 'last_name', 'phone_number', 'email', 'region', 'city', 'address', 'post_office_number'
        )

    def clean_post_office_number(self):
        if self.cleaned_data['post_office_number'] and self.cleaned_data['post_office_number'] <= 0:
            raise forms.ValidationError("Номер почтового отделения не может быть отрицательным")
        elif self.cleaned_data['post_office_number'] == 0:
            raise forms.ValidationError("Номер почтового отделения не может быть равным нулю")
        return self.cleaned_data['post_office_number']

    def clean_first_name(self):
        if not self.cleaned_data['first_name']:
            raise forms.ValidationError("Введите ваше имя")
        return self.cleaned_data['first_name']

    def clean_last_name(self):
        if not self.cleaned_data['last_name']:
            raise forms.ValidationError("Введите вашу фамилию")
        return self.cleaned_data['last_name']


class OrderingForm(ProfileForm):
    delivery_method = forms.ChoiceField(choices=Order.DELIVERY_METHOD_CHOICE, label="Способ доставки")
    first_name = forms.CharField(label='Имя', validators=[RegexValidator(regex=r'^[a-zA-Zа-яА-Я]+$')])
    last_name = forms.CharField(label='Фамилия', validators=[RegexValidator(regex=r'^[a-zA-Zа-яА-Я]+$')])
    region = forms.CharField(label='Область', validators=[RegexValidator(regex=r'^[a-zA-Zа-яА-Я]+$')])
    city = forms.CharField(label='Город', validators=[RegexValidator(regex=r'^[a-zA-Zа-яА-Я]+$')])

    class Meta:
        model = CustomUser
        fields = (
            'first_name', 'last_name', 'phone_number', 'email', 'region', 'city', 'delivery_method', 'address',
            'post_office_number',
        )

    def clean_address(self):
        if not self.cleaned_data['address'] and self.cleaned_data['delivery_method'] == '2':
            raise forms.ValidationError("Для доставки курьером необходимо ввести адрес доставки")
        return self.cleaned_data['address']

    def clean_post_office_number(self):
        if not self.cleaned_data['post_office_number'] and self.cleaned_data['delivery_method'] == '1':
            raise forms.ValidationError("Для доставки почтой необходимо ввести номер почтового отделения")
        elif self.cleaned_data['post_office_number'] and self.cleaned_data['post_office_number'] <= 0:
            raise forms.ValidationError("Номер почтового отделения не может быть отрицательным")
        return self.cleaned_data['post_office_number']


class ReviewForm(forms.ModelForm):
    text = forms.CharField(label='Написать отзыв...', widget=forms.Textarea,)

    class Meta:
        model = Reviews
        fields = (
            'text',
        )

    def clean_text(self):
        if not self.cleaned_data['text']:
            raise forms.ValidationError("Введите ваш отзыв")
        if len(self.cleaned_data['text']) > 512:
            raise forms.ValidationError("Максимальное количество символов 512")
        return self.cleaned_data['text']
