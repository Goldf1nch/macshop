from django.shortcuts import redirect
from django.views.generic import TemplateView, View, DeleteView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect

from re import findall

from app.models import MacBook, OneClickPurchaseRequisitions, Cart, CustomUser, Order, Reviews
from app.forms import SimpleBuyForm, ProfileForm, OrderingForm, ReviewForm
from .services import _get_macbook, _get_categories, _get_years, _get_filtered_macbooks, _get_tags, \
    _get_current_cart_products, _get_current_cart_final_price, _get_reviews, _get_orders_history, _create_order


class CartView(TemplateView):
    """Корзина"""

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['cart'] = _get_current_cart_products(user_id=self.request.user.id)
            context['final_price'] = _get_current_cart_final_price(user_id=self.request.user.id)
        return context


class Index(CartView):
    """Главная страница"""
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['categories'] = _get_categories()
        return context


class ProductsView(CartView):
    """Список продутов"""
    template_name = 'products/products_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProductsView, self).get_context_data(**kwargs)
        context['macbooks'] = _get_macbook(pk=kwargs['pk'])
        context['years'] = _get_years()
        context['tags'] = _get_tags()
        return context


class FilterMacBookView(CartView):
    """Фильтр """
    template_name = 'products/filtered_products.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['years'] = _get_years()
        context['tags'] = _get_tags()
        context['path'] = findall(r'\d', self.request.GET.getlist('path')[0])[0]
        if self.request.GET.getlist('order_by'):
            order_by = self.request.GET.getlist('order_by')[0]
        else:
            order_by = 0
        context['queryset'] = _get_filtered_macbooks(
            cpu=self.request.GET.getlist("cpu"),
            display=self.request.GET.getlist("display"),
            ram=self.request.GET.getlist("ram"),
            ssd=self.request.GET.getlist("ssd"),
            hdd=self.request.GET.getlist("hdd"),
            condition=self.request.GET.getlist("condition"),
            year=self.request.GET.getlist("year"),
            tags=self.request.GET.getlist("tags"),
            category_id=context['path'],
            order_by=order_by)
        return context


class ProductDetailView(CartView):
    """Страница продукта"""
    template_name = "products/product_detail.html"

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        context['mac'] = MacBook.objects.get(id=kwargs['pk'])
        return context


class SimpleBuyView(CartView):
    """Страница подачи заявки на покупку в один клилк"""
    template_name = "purchase/simple_buy.html"

    def get_context_data(self, **kwargs):
        context = super(SimpleBuyView, self).get_context_data(**kwargs)
        context['form'] = SimpleBuyForm()
        context['mac'] = MacBook.objects.get(id=kwargs['pk'])
        return context

    def post(self, request, *args, **kwargs):
        form = SimpleBuyForm(data=request.POST)
        if form.is_valid():
            simple_buy = OneClickPurchaseRequisitions(phone_number=request.POST['phone_number'],
                                                      product=MacBook.objects.get(id=kwargs['pk']))
            simple_buy.save()
            return redirect('confirm')
        context = self.get_context_data(pk=kwargs['pk'])
        context['form'] = form
        return self.render_to_response(context)


class AddToCartView(LoginRequiredMixin, CartView, View):
    """Добавить в корзину"""

    def get(self, request, *args, **kwargs):
        user = self.request.user
        if user:
            product = MacBook.objects.get(id=kwargs['pk'])
            cart = Cart(user=user, product=product)
            cart.save()
            return redirect(self.request.META['HTTP_REFERER'])


class CartItemRemoveView(DeleteView):
    """Удалить из корзины"""

    def delete(self, *args, **kwargs):
        product = Cart.objects.get(id=self.request.POST['id'])
        product.delete()
        return HttpResponseRedirect(self.request.META['HTTP_REFERER'])


class OrderingView(LoginRequiredMixin, CreateView):
    """Подтверждение заказа"""
    template_name = "purchase/ordering.html"
    model = CustomUser
    form_class = OrderingForm

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['cart'] = _get_current_cart_products(user_id=self.request.user.id)
            context['final_price'] = _get_current_cart_final_price(user_id=self.request.user.id)
        return context

    def get_form_kwargs(self):
        kwargs = super(OrderingView, self).get_form_kwargs()
        kwargs['instance'] = CustomUser.objects.get(id=self.request.user.id)
        return kwargs

    def form_valid(self, form):
        if Cart.objects.filter(user=self.request.user, processed=False):
            _create_order(self.request)
            return redirect('confirm')
        else:
            return redirect('/')


class ReviewsView(CartView):
    """Отзывы"""
    template_name = "reviews.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = ReviewForm()
        context['reviews'] = _get_reviews()
        return context

    def post(self, *args, **kwargs):
        form = ReviewForm(data=self.request.POST)
        if form.is_valid():
            review = Reviews(username=self.request.user, text=self.request.POST['text'])
            if Order.objects.filter(user_id=self.request.user.id, in_order=True):
                if self.request.POST.getlist('stars'):
                    review.rating_star = self.request.POST['stars'][-1]
                    review.save()
                else:
                    context = self.get_context_data()
                    context['form'] = form
                    context['error'] = "Оцените пожалуйста ваш опыт покупки от 1 до 5"
                    return self.render_to_response(context)
            else:
                context = self.get_context_data()
                context['form'] = form
                context['error'] = "Отзывы могут оставлять только те пользователи, которые уже купили товар"
                return self.render_to_response(context)
        return redirect('reviews')


class ProfileView(LoginRequiredMixin, CreateView):
    """Личный кабинет пользоавателя"""
    template_name = "profile/profile.html"
    model = CustomUser
    form_class = ProfileForm

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['cart'] = _get_current_cart_products(user_id=self.request.user.id)
            context['final_price'] = _get_current_cart_final_price(user_id=self.request.user.id)
        return context

    def get_form_kwargs(self):
        kwargs = super(ProfileView, self).get_form_kwargs()
        kwargs['instance'] = CustomUser.objects.get(id=self.request.user.id)
        return kwargs

    def form_valid(self, form):
        form.save()
        return redirect('profile')


class PurchaseHistoryView(CartView):
    """История заказов пользователя"""
    template_name = 'profile/purchase_history.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['orders'] = _get_orders_history(self.request.user)
        return context


class ConfirmView(CartView):
    """Заявка подтверждена"""
    template_name = "purchase/confirmed.html"


class HelpView(CartView):
    """Вспомогательная информация"""
    template_name = "help.html"


class ContactsView(CartView):
    """Контактная информация"""
    template_name = "contacts.html"
