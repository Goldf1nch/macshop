from django.db import models

from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType


class CustomUser(AbstractUser):
    """Пользователь"""
    region = models.CharField("Область", max_length=127, null=True)
    city = models.CharField("Город", max_length=127, null=True)
    address = models.CharField("Адрес доставки", max_length=127, null=True)
    phone_number = models.CharField("Мобильный телефон", max_length=12, null=True)
    post_office_number = models.IntegerField("Номер почтового отделения", null=True)


class CreatedAt(models.Model):
    """"Дата создания"""
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Category(models.Model):
    """Модель(тип) MacBook'a"""
    MODEL_AIR = 1
    MODEL_PRO = 2

    MODEL_CHOICES = (
        (MODEL_AIR, 'MacBook Air'),
        (MODEL_PRO, 'MacBook Pro'),
    )
    model = models.PositiveSmallIntegerField("Модель MacBook'a", choices=MODEL_CHOICES, null=True)
    short_description = models.CharField("Краткое описание", max_length=355)
    image = models.ImageField("Изображение", upload_to="categories/")
    max_cpu = models.CharField("Максимальный процессор", max_length=255, null=True,
                               help_text='Введите максимально возможный процессор'
                                         ' для этой модели MacBook')
    max_ram = models.PositiveIntegerField("Максимальный объем оперативной памяти", default=8,
                                          help_text='Введите максимально возможный объем оперативной памяти'
                                                    ' для этой модели MacBook')
    max_hdd = models.CharField("Максимальный объем жесткого диска", default=0, max_length=63,
                               help_text='Введите максимально возможный объем жесткого диска'
                                         ' для этой модели MacBook с указанием единиц измерения')
    max_ssd = models.CharField("Максимальный объем SSD", default=0, max_length=63,
                               help_text='Введите максимально возможный объем SSD'
                                         ' для этой модели MacBook с указанием единиц измерения')
    display = models.PositiveIntegerField("Диагональ экрана", default=13)

    def __str__(self):
        return str(f'{self.get_model_display()} {self.display}')

    class Meta:
        verbose_name = "Модель MacBook'a"
        verbose_name_plural = "Модель MacBook'a"


class Tag(models.Model):
    """Теги"""
    tag = models.CharField("Теги", max_length=255)

    def __str__(self):
        return self.tag

    class Meta:
        verbose_name = "Теги"
        verbose_name_plural = "Теги"


class MacBook(CreatedAt):
    """"MacBook"""
    CONDITION_NEW = 1
    CONDITION_GREAT = 2
    CONDITION_GOOD = 3
    CONDITION_NORMAL = 4
    CONDITION_BAD = 5

    CONDITION_CHOICES = (
        (CONDITION_NEW, "Новый"),
        (CONDITION_GREAT, "Отличное"),
        (CONDITION_GOOD, "Хорошее"),
        (CONDITION_NORMAL, "Нормальное"),
        (CONDITION_BAD, "Плохое"),
    )
    name = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.TextField("Описание", max_length=1023)
    price = models.PositiveIntegerField("Цена", null=True)
    ram = models.PositiveIntegerField("Объем оперативной памяти", default=8)
    hdd = models.CharField("Объем жесткого диска", default=0, max_length=63,
                           help_text='Введите объем с указанием единиц измерения')
    ssd = models.CharField("Объем SSD", default=0, max_length=63,
                           help_text='Введите объем с указанием единиц измерения')
    battery_life = models.PositiveIntegerField("Количество циклов зарядки", default=0)
    display = models.PositiveIntegerField("Диагональ экрана", default=13)
    cpu = models.CharField("Процессор", max_length=255, null=True)
    gpu = models.CharField("Видеокарта", max_length=255, null=True)
    os = models.CharField("Версия ОС", max_length=255, null=True)
    tags = models.ManyToManyField(Tag, blank=True)
    complete_set = models.CharField("Комплектация", max_length=255, null=True)
    condition = models.PositiveSmallIntegerField("Состояние", choices=CONDITION_CHOICES, default=None)
    year = models.PositiveIntegerField("Год выпуска", default=2020)
    colour = models.CharField("Цвет", max_length=63, default="Серый", null=True)
    availability = models.BooleanField("Наличие товара", default=False)

    def __str__(self):
        return f'{str(self.name)} {self.year}'


class MacBookImage(models.Model):
    """Картинки MacBook`ов"""
    image = models.ImageField("Изображение", upload_to="macbook_images/")
    macbook = models.ForeignKey(MacBook, verbose_name="Выберите для какого MacBook это фото", on_delete=models.CASCADE,
                                help_text='Выберите для какого MacBook это фото')

    def __str__(self):
        return f'{str(self.macbook)} image'

    class Meta:
        verbose_name = "Фото MacBook"
        verbose_name_plural = "Фото MacBook"


class OneClickPurchaseRequisitions(CreatedAt):
    """Номер телефона покупателя через покупу в один клик"""
    phone_number = models.CharField("Мобильный телефон", max_length=12, null=True)
    product = models.ForeignKey(MacBook, verbose_name="Желаемый ноутбук", on_delete=models.CASCADE)
    processed = models.BooleanField("Обработано", default=False)

    def __str__(self):
        return f'Заявка на покупку №{self.id}'

    class Meta:
        verbose_name = "Заявки на покупку в один клик"
        verbose_name_plural = "Заявки на покупку в один клик"


class Cart(models.Model):
    """Корзина"""

    user = models.ForeignKey(CustomUser, verbose_name='Покупатель', on_delete=models.CASCADE)
    product = models.ForeignKey(MacBook, verbose_name='Продукт', on_delete=models.CASCADE)
    processed = models.BooleanField("Обработано", default=False)

    def __str__(self):
        return f'{self.product.name} id {self.product.id} из корзины'

    class Meta:
        verbose_name = "Товар в корзине"
        verbose_name_plural = "Товар в корзине"


class Order(CreatedAt):
    """Готовый заказ"""

    MAIL_DELIVERY = 1
    COURIER_DELIVERY = 2

    DELIVERY_METHOD_CHOICE = (
        (MAIL_DELIVERY, "Доставка почтой"),
        (COURIER_DELIVERY, "Доставка курьером"),
    )

    user = models.ForeignKey(CustomUser, verbose_name='Покупатель', on_delete=models.CASCADE)
    products = models.ManyToManyField(Cart, verbose_name='Продукты', default=None)
    delivery_method = models.PositiveSmallIntegerField("Способ доставки", choices=DELIVERY_METHOD_CHOICE, default=1)
    final_price = models.FloatField("Цена")
    first_name = models.CharField("Имя", max_length=127, null=True)
    last_name = models.CharField("Фамилия", max_length=127, null=True)
    region = models.CharField("Область", max_length=127, null=True)
    city = models.CharField("Город", max_length=127, null=True)
    address = models.CharField("Адрес доставки", max_length=127, null=True, blank=True)
    phone_number = models.CharField("Мобильный телефон", max_length=12, null=True)
    post_office_number = models.IntegerField("Номер почтового отделения", null=True, blank=True)
    in_order = models.BooleanField('Оброботано', default=False)

    def __str__(self):
        return f'Заказ № {self.id}'

    class Meta:
        verbose_name = "Заказы"
        verbose_name_plural = "Заказы"


class Reviews(CreatedAt):
    """Отзывы"""

    username = models.ForeignKey(CustomUser, verbose_name='Покупатель', on_delete=models.CASCADE)
    text = models.CharField("Комментарий", max_length=512)
    rating_star = models.SmallIntegerField("Звезды", default=0)

    def __str__(self):
        return f'Отзыв пользователя {self.username}'

    class Meta:
        verbose_name = "Отзыв"
        verbose_name_plural = "Отзывы"
