from django.contrib import admin
from django import forms


from app.models import Category, MacBook, Tag, MacBookImage, OneClickPurchaseRequisitions, Order, Cart, CustomUser, \
    Reviews


class CategoryAdminForm(forms.ModelForm):
    short_description = forms.CharField(widget=forms.Textarea)


class CategoryAdmin(admin.ModelAdmin):
    form = CategoryAdminForm


@admin.register(MacBook)
class MacBookAdmin(admin.ModelAdmin):
    list_display = ('name', 'availability')
    list_filter = ('availability',)


@admin.register(OneClickPurchaseRequisitions)
class SimpleBuyAdmin(admin.ModelAdmin):
    """Заявки на покупку в один клик"""

    list_display = ('product', 'processed', 'created_at')
    list_filter = ('processed',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'user', 'in_order', 'created_at')
    list_filter = ('in_order',)


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = ('user', 'processed')
    list_filter = ('processed',)


admin.site.register(Category, CategoryAdmin)
admin.site.register(CustomUser)
admin.site.register(Tag)
admin.site.register(MacBookImage)
admin.site.register(Reviews)
